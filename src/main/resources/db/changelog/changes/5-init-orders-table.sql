
CREATE TABLE orders
(
    id               uuid PRIMARY KEY,
    date_of_closing  DATE,
    date_of_creation DATE,
    name             VARCHAR,
    quantity         DOUBLE PRECISION,
    user_id          uuid,
    CONSTRAINT fk_users
        foreign key (user_id)
            references users (id)
)