
CREATE TABLE wallet_users_crypto_coin_list
(
    wallet_id uuid,
    id        VARCHAR,
    name      VARCHAR,
    quantity  DOUBLE PRECISION,
    CONSTRAINT fk_wallet
        foreign key (wallet_id)
            references wallets (id)
);