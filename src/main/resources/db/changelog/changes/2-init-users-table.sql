
CREATE TABLE users
(
    id        uuid PRIMARY KEY,
    username  VARCHAR not null,
    password  VARCHAR not null,
    wallet_id uuid,
    role      VARCHAR,
    CONSTRAINT fk_wallet
        foreign key (wallet_id)
            references wallets (id)
);