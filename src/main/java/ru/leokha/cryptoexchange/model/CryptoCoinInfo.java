package ru.leokha.cryptoexchange.model;

import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.io.Serializable;

@Entity
@Embeddable
@Table(name = "coins")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CryptoCoinInfo implements Serializable {
    @Id
    private String id;
    private String name;
}
