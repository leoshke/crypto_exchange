package ru.leokha.cryptoexchange.model;

import jakarta.persistence.Embeddable;
import lombok.*;

import java.io.Serializable;

@Embeddable
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CryptoCoin implements Serializable {
    private CryptoCoinInfo coinInfo;
    private Double quantity;

}
