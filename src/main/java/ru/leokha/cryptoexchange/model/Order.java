package ru.leokha.cryptoexchange.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import ru.leokha.cryptoexchange.enums.OrderType;

import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    @Enumerated(EnumType.STRING)
    private OrderType orderType;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
    @Embedded
    private CryptoCoin orderInfo;
    @CreationTimestamp
    private LocalDate dateOfCreation;
    private LocalDate dateOfClosing;

    private boolean isActive;

}
