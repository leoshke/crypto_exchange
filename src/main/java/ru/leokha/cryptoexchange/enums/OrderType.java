package ru.leokha.cryptoexchange.enums;

public enum OrderType {
    SELL_REQUEST,
    BUY_REQUEST
}
