package ru.leokha.cryptoexchange.service;

import ru.leokha.cryptoexchange.model.CryptoCoin;
import ru.leokha.cryptoexchange.model.Wallet;

public interface WalletService {
    void updateUserWallet(String username, CryptoCoin cryptoCoin);
    Wallet getWallet(String username);
}
