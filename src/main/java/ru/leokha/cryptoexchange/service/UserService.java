package ru.leokha.cryptoexchange.service;

import jakarta.persistence.EntityNotFoundException;
import ru.leokha.cryptoexchange.dto.AuthenticationOrRegisterRequest;
import ru.leokha.cryptoexchange.dto.AuthenticationResponse;
import ru.leokha.cryptoexchange.dto.UserDTO;
import ru.leokha.cryptoexchange.model.User;

public interface UserService {
    public UserDTO getUserByUsername(String username);

    public UserDTO saveUser(UserDTO user);

    public AuthenticationResponse register(AuthenticationOrRegisterRequest request);

    public AuthenticationResponse authenticate(AuthenticationOrRegisterRequest request);
}
