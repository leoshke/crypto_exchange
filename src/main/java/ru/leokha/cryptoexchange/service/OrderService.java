package ru.leokha.cryptoexchange.service;

import ru.leokha.cryptoexchange.model.Order;

import java.util.List;
import java.util.UUID;

public interface OrderService {
    List<Order> getOrderHistoryForUser(UUID id);
}
