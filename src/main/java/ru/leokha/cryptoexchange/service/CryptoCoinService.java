package ru.leokha.cryptoexchange.service;

import ru.leokha.cryptoexchange.model.CryptoCoinInfo;

import java.util.List;
public interface CryptoCoinService {

    double getRateForCoin(String id);

    List<CryptoCoinInfo> getAllCoins();

    boolean isCoinSupported(String coinId);
}
