package ru.leokha.cryptoexchange.service.impl;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.leokha.cryptoexchange.model.CryptoCoinInfo;
import ru.leokha.cryptoexchange.repository.CryptoCoinRepository;
import ru.leokha.cryptoexchange.service.CryptoCoinService;

import java.util.List;
import java.util.logging.Logger;

@Service
@Primary
@Slf4j
@RequiredArgsConstructor
public class CryptoCoinServiceImpl implements CryptoCoinService {
    private final RestTemplate restTemplate;
    private final CryptoCoinRepository cryptoCoinRepository;

    @Override
    public double getRateForCoin(String id) {
        var result = restTemplate.getForObject("https://api.coingecko.com/api/v3/simple/price?ids=the-open-network&vs_currencies=rub",
                String.class);
        log.info(result);
        return 0;
    }

    @Override
    public List<CryptoCoinInfo> getAllCoins() {
        return cryptoCoinRepository.findAll();
    }

    public boolean isCoinSupported(String coinId) {
        return cryptoCoinRepository.findById(coinId).isPresent();
    }

    @PostConstruct //TODO: REMOVE
    public void fillDbWithCoins() {
        CryptoCoinInfo ci = new CryptoCoinInfo("btc", "bitcoin");
        CryptoCoinInfo ci1 = new CryptoCoinInfo("eth", "etherium");

        cryptoCoinRepository.save(ci);
        cryptoCoinRepository.save(ci1);
    }
}
