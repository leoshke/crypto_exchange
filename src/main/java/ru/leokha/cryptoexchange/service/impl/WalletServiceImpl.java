package ru.leokha.cryptoexchange.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leokha.cryptoexchange.model.CryptoCoin;
import ru.leokha.cryptoexchange.model.Wallet;
import ru.leokha.cryptoexchange.repository.WalletRepository;
import ru.leokha.cryptoexchange.service.CryptoCoinService;
import ru.leokha.cryptoexchange.service.MyUserDetailsService;
import ru.leokha.cryptoexchange.service.WalletService;
import ru.leokha.cryptoexchange.service.impl.UserServiceImpl;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    private final CryptoCoinService cryptoCoinService;

    private final MyUserDetailsService myUserDetailsService;

    public void updateUserWallet(String username, CryptoCoin cryptoCoin) {

        if (!cryptoCoinService.isCoinSupported(cryptoCoin.getCoinInfo().getId()))
            throw new IllegalArgumentException
                    ("Exchange does not support coin with id " + cryptoCoin.getCoinInfo().getId());

        Wallet wallet = myUserDetailsService.loadUserByUsername(username).getWallet();

        Optional<CryptoCoin> coinToUpdate = wallet.getUsersCryptoCoinList().stream()
                .filter(coin -> coin.getCoinInfo().equals(cryptoCoin.getCoinInfo()))
                .findFirst();

        //if coin exists in user's wallet, updating it
        if (coinToUpdate.isPresent())
            walletRepository.updateWalletCoin(
                    cryptoCoin.getQuantity() + coinToUpdate.get().getQuantity(),
                     cryptoCoin.getCoinInfo().getId());
        //if not, creating new record for wallet's coins
        else walletRepository.createNewCoinInWallet(
                wallet.getId(),
                cryptoCoin.getCoinInfo().getId(),
                cryptoCoin.getCoinInfo().getName(),
                cryptoCoin.getQuantity());
    }

    public Wallet getWallet(String username) {
        return myUserDetailsService.loadUserByUsername(username).getWallet();
    }
}
