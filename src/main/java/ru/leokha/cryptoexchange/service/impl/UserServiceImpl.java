package ru.leokha.cryptoexchange.service.impl;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.leokha.cryptoexchange.dto.AuthenticationOrRegisterRequest;
import ru.leokha.cryptoexchange.dto.AuthenticationResponse;
import ru.leokha.cryptoexchange.dto.UserDTO;
import ru.leokha.cryptoexchange.enums.Role;
import ru.leokha.cryptoexchange.mapper.UserMapper;
import ru.leokha.cryptoexchange.model.User;
import ru.leokha.cryptoexchange.model.Wallet;
import ru.leokha.cryptoexchange.repository.UserRepository;
import ru.leokha.cryptoexchange.service.JwtService;
import ru.leokha.cryptoexchange.service.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    public UserDTO getUserByUsername(String username) {
        User user = userRepository.findUserByUsername(username).orElseThrow(EntityNotFoundException::new);
        System.out.println(user.getId());
        System.out.println(user.getUsername());
        return UserMapper.INSTANCE.userToDto(user);
    }

    public UserDTO saveUser(UserDTO user) {
        userRepository.save(UserMapper.INSTANCE.dtoToUser(user));
        return user; //TODO: method may be void
    }

    public AuthenticationResponse register(AuthenticationOrRegisterRequest request) {
        User user = User.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .wallet(new Wallet())
                .role(Role.ROLE_USER)
                .build();

        userRepository.save(user);

        String jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationOrRegisterRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        User user = userRepository.findUserByUsername(request.getUsername()).orElseThrow(() -> new UsernameNotFoundException("No such user found"));
        String jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
