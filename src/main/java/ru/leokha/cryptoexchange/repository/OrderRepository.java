package ru.leokha.cryptoexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leokha.cryptoexchange.model.Order;

import java.util.List;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {

    List<Order> getOrdersByIsActiveIsTrue();
}
