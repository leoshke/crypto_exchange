package ru.leokha.cryptoexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leokha.cryptoexchange.model.CryptoCoinInfo;

import java.util.UUID;

public interface CryptoCoinRepository extends JpaRepository<CryptoCoinInfo, String> {
}
