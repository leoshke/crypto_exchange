package ru.leokha.cryptoexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.leokha.cryptoexchange.model.CryptoCoin;
import ru.leokha.cryptoexchange.model.Wallet;

import java.util.UUID;

public interface WalletRepository extends JpaRepository<Wallet, UUID> {
    @Modifying
    @Query(value = "UPDATE wallet_users_crypto_coin_list SET quantity= ?1 WHERE id=?2", nativeQuery = true )
    void updateWalletCoin(double quantity, String coinId);

    @Modifying
    @Query(value = "INSERT INTO wallet_users_crypto_coin_list(wallet_id, id, name, quantity) VALUES (?1, ?2, ?3, ?4)", nativeQuery = true)
    void createNewCoinInWallet(UUID walletId, String coinId, String coinName, double quantity);
}
