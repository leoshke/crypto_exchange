package ru.leokha.cryptoexchange.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.leokha.cryptoexchange.dto.UserDTO;
import ru.leokha.cryptoexchange.model.User;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDTO userToDto (User user);

    User dtoToUser (UserDTO userDTO);
}
