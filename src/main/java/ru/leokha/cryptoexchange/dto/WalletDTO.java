package ru.leokha.cryptoexchange.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.leokha.cryptoexchange.model.CryptoCoin;
import ru.leokha.cryptoexchange.model.User;

import java.util.List;
import java.util.UUID;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WalletDTO {
    private UUID id;
    private List<CryptoCoinDTO> UsersCryptoCoinList;
}
