package ru.leokha.cryptoexchange.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.leokha.cryptoexchange.enums.OrderType;
import ru.leokha.cryptoexchange.model.CryptoCoin;
import ru.leokha.cryptoexchange.model.User;

import java.time.LocalDate;
import java.util.UUID;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private UUID id;
    private OrderType orderType;
    private CryptoCoinDTO offer;
    private LocalDate dateOfCreation;
    private LocalDate dateOfClosing;
    private boolean isActive;
}
