package ru.leokha.cryptoexchange.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.leokha.cryptoexchange.model.Order;
import ru.leokha.cryptoexchange.model.User;
import ru.leokha.cryptoexchange.repository.OrderRepository;
import ru.leokha.cryptoexchange.repository.UserRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/order")
@RequiredArgsConstructor
@SecurityRequirement(name = "/exchange")
public class OrderController {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;


    /**
     Get list of active orders
     */
    @GetMapping("/")
    public List<Order> getAll() {
        return orderRepository.getOrdersByIsActiveIsTrue();
    }
    @PostMapping("/publish")
    public Order publishNewOrder(@RequestBody Order order) {
        return null;
    }

    @PatchMapping("/{orderId}")
    public Order completeOrder(@PathVariable UUID orderId) {
        return null;
    }
}
