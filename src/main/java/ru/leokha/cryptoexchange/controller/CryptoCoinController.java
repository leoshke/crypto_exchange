//package ru.leokha.cryptoexchange.controller;
//
//import jakarta.persistence.EntityNotFoundException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ru.leokha.cryptoexchange.model.CryptoCoin;
//import ru.leokha.cryptoexchange.model.CryptoCoinInfo;
//import ru.leokha.cryptoexchange.repository.CryptoCoinRepository;
//import io.swagger.v3.oas.annotations.Operation;
//
//import java.util.List;
//
//@RequestMapping("pro-api.coingecko.com/api/v3")
//@RestController
//public class CryptoCoinController {
//    @Autowired
//    private CryptoCoinRepository cryptoCoinRepository;
//    @GetMapping("/")
//    @Operation(summary = "Получить список всех криптовалют")
//
//    public List<CryptoCoinInfo> getAllCryptoCoin() {
//        return cryptoCoinRepository.findAll();
//    }
//    @GetMapping("/{id}")
//    @Operation(summary = "Получить криптовалюту по id")
//    public CryptoCoin getCryptoCoinById(@PathVariable String id) {
//        return cryptoCoinRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("This CryptoCoin not found"));
//    }
//    @PutMapping("/")
//    @Operation(summary = "Добавить новую криптовалюту или обновить существующую", description = "Для обновления существующей криптовалюты необходимо отправить криптовалюту в JSON формате")
//    public CryptoCoin addOrUpdateCryptoCoin(@RequestBody CryptoCoin cryptoCoin) {
//        return cryptoCoinRepository.saveAllAndFlush(cryptoCoin);
//    }
//    @DeleteMapping("/{id}")
//    @Operation(summary = "Удалить криптовалюту по id")
//    public void deleteCryptoCoin(@PathVariable String id){
//        cryptoCoinRepository.deleteById(id);
//    }
//
//}
