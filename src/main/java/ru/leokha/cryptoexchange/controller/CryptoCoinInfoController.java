package ru.leokha.cryptoexchange.controller;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.leokha.cryptoexchange.model.CryptoCoinInfo;
import ru.leokha.cryptoexchange.repository.CryptoCoinRepository;

import java.util.List;

@RestController
@RequestMapping("/api/v1/crypto-coins")
@RequiredArgsConstructor
public class CryptoCoinInfoController {
    private final CryptoCoinRepository cryptoCoinRepository;


    /**
     Get all crypto coins which our exchange supports
     */
    @GetMapping("/")
    public List<CryptoCoinInfo> getAllCoins() {
        return cryptoCoinRepository.findAll(); //TODO:REPO CALL FROM CONTROLLER, ALERT
    }

}
