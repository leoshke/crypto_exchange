package ru.leokha.cryptoexchange.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.leokha.cryptoexchange.model.CryptoCoin;
import ru.leokha.cryptoexchange.model.Wallet;
import ru.leokha.cryptoexchange.service.impl.WalletServiceImpl;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/wallet")
@RequiredArgsConstructor
@SecurityRequirement(name = "/exchange")
public class WalletController {
    private final WalletServiceImpl walletService;

    @GetMapping("/")
    public Wallet getWalletDetails(Principal principal) {
        return walletService.getWallet(principal.getName());
    }

    @PatchMapping("/")
    public void fillWallet(Principal principal, @RequestBody CryptoCoin cryptoCoin) {
        String currentUsername = principal.getName();
        walletService.updateUserWallet(currentUsername, cryptoCoin);
    }
}
